(function ($) {
  Drupal.behaviors.themeToast = {
    attach: function () {
      $('.toast').toast('show');
    }
  }

  Drupal.behaviors.general = {
    attach: function (context, settings) {
      $( '.js-btn-close', context ).on( 'click', function() {
        $('.collapse').collapse('hide');
      });

      $( '.input-style-checkbox', context ).on( 'click', function() {
        var checkBoxes = $( this ).siblings('input');
        checkBoxes.prop("checked", !checkBoxes.prop("checked"));
      });

      $('.dropdown-submenu a', context).on("click", function(e) {
        if ($(window).width() < 991) {
          var submenu = $(this);

          if (submenu.next('.dropdown-menu').hasClass('show')) {
            e.stopPropagation();
          } else {
            submenu.next('.dropdown-menu').addClass('show');
            submenu.parent().siblings().find('.dropdown-menu').removeClass('show');
            e.stopPropagation();
            e.preventDefault();
          }
        } else {
          e.stopPropagation();
        }
      });

      $( window ).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 5) {
          if ($("#main-navbar").hasClass('navbar-light')) {
            $("#main-navbar").removeClass("navbar-light").addClass("navbar-dark bg-white");
            $(".main-logo").attr("src", $(".main-logo").attr('src').replace('logo.svg', 'logo-dark.svg'));
          }
        } else {
          if ($("#main-navbar").hasClass('navbar-dark')) {
            $("#main-navbar").addClass("navbar-light").removeClass("navbar-dark bg-white");
            $(".main-logo").attr("src", $(".main-logo").attr('src').replace('logo-dark.svg', 'logo.svg'));
          }
        }
      });
    }
  };
})(jQuery);
