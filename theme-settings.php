<?php

/**
 * Implementation of hook_form_system_theme_settings_alter()
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function optasyadmin_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['main-tabs'] = [
    '#type' => 'vertical_tabs',
    '#prefix' => '<h2><small>' . t('Theme Settings') . '</small></h2>',
    '#weight' => -10,
  ];

  // Layout.
  $form['layout'] = array(
    '#type' => 'details',
    '#title' => t('Layout'),
    '#group' => 'main-tabs',
  );

    // List of regions
    $theme = \Drupal::theme()->getActiveTheme()->getName();
    $region_list = system_region_list($theme, $show = REGIONS_ALL);

    $form['layout']['layout'] = array(
      '#type' => 'details',
      '#title' => t('Base layout'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['layout']['layout']['layout_type'] = array(
      '#title' => t('Select base layout'),
      '#type' => 'radios',
      '#default_value' => theme_get_setting('layout_type'),
      '#options' => array(
        'default' => t('Default'),
        'admin' => t('Admin'),
      ),
      '#attributes' => array (
        'class' => array (
          'radioTabs',
        ),
      ),
    );

    //Region
    $form['layout']['region'] = array(
      '#type' => 'details',
      '#title' => t('Region'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    foreach ($region_list as $name => $description) {
      $form['layout']['region'][$name] = array(
        '#type' => 'details',
        '#title' => $description,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      $form['layout']['region'][$name]['theme_region_class_' . $name] = array(
        '#type' => 'textfield',
        '#title' => t('Classes for @description region', array('@description' => $description)),
        '#default_value' => theme_get_setting('theme_region_class_' . $name),
        '#size' => 40,
      );
    }

/*  $form['newtab'] = array(
    '#type'         => 'details',
    '#title'        => t('Tab title'),
    '#description'  => t('Tab description'),
    '#weight' => 10,
    '#group' => 'main-group',
  );

    $form['newtab']['browsersync'] = array(
      '#type'         => 'checkbox',
      '#title'        => t('Activete BrowserSync'),
      '#default_value' => theme_get_setting('browsersync'),
      '#description'  => t('Check this option if you\'d like to show the My Theme navbar.'),
    );*/
}