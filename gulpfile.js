"use strict";

// Load plugins
const gulp = require("gulp");
const sass = require("gulp-sass");
const plumber = require("gulp-plumber");
const browsersync = require("browser-sync").create();
const sourcemaps    = require('gulp-sourcemaps');
const autoprefixer = require("autoprefixer");
const strip = require('gulp-strip-comments');
const concat = require('gulp-concat');

const cssnano = require("cssnano");
const postcss = require("gulp-postcss");

// BrowserSync
function browserSync(done) {
  browsersync.init({
    proxy: "http://optasyadmin/"
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// CSS task
function css() {
  return gulp
  .src("./dev/scss/**/*.scss")
  .on('error', catchErr)
  .pipe(sourcemaps.init())
  .pipe(plumber())
  .pipe(sass({ outputStyle: "expanded" }, {errLogToConsole: true}))
  .on('error', catchErr)
  .pipe(postcss([autoprefixer]))
  .pipe(sourcemaps.write("./map"))
  .pipe(gulp.dest("./css"))
  .pipe(browsersync.stream({match: '**/*.css'}));
}

function cssMin() {
  return gulp
    .src("./dev/scss/**/*.scss")
    .pipe(plumber())
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(postcss([autoprefixer]))
    .pipe(gulp.dest("./css"))
}

// Transpile, concatenate and minify scripts
function scripts() {
  return (
    gulp
      .src(["./dev/js/**/*"])
      .pipe(gulp.dest("./js"))
      .pipe(browsersync.stream())
  );
}

function bootstrapJs() {
  return (
    gulp
    .src(["./node_modules/bootstrap/dist/js/bootstrap.min.js", "./node_modules/popper.js/dist/umd/popper.min.js"])
    .pipe(strip())
    .pipe(gulp.dest("./js"))
  );
}

// Watch files
function watchFiles() {
  gulp.watch("./dev/scss/**/*", css);
  gulp.watch("./dev/js/**/*", scripts);
  gulp.watch("./templates/**/*", browserSyncReload);
}

// define complex tasks
const build = gulp.series(gulp.parallel(cssMin, scripts, bootstrapJs));
const watch = gulp.parallel(watchFiles, browserSync);

function catchErr(e) {
  console.log(e);
  this.emit('end');
}

// export tasks
exports.watch = watch;
exports.default = build;
