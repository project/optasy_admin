INTRODUCTION
------------
# Optasy Admin
Administration theme based on [Bootstrap 4](https://www.drupal.org/project/bootstrap)
for Drupal 8.

REQUIREMENTS
------------
## No requirements

INSTALLATION
------------
## Installation & Use
1. Download and enable the Theme.
2. Enable Optasy Admin theme and setup this as Administration theme on the path
"/admin/appearance".

CONFIGURATION
-------------
No custom configuration required.

TROUBLESHOOTING
---------------
[Check the issue queue](https://www.drupal.org/project/issues/optasy_admin)

CONTRIBUTING
---------------
The theme styles are compiled using the Gulp SASS compiler, this will require:
  * Install package:
    1. command: **npm install -g gulp** - install global gulp if not installed
    yet.

    2. command: **npm install** - should be ran in the theme root folder, the
    files should be here gulpfile.js and package.json.

    3. command: **npm install bootstrap** - install the lastest version of
    [Bootstrap 4](http://getbootstrap.com/)

  * Use Gulp:
    1. command: **gulp** - run gulp and all services.

  * Update gulp
    1. command: **npm outdated** - show all modules which require an update.

    2. Need to do this modification in package.json
       Example:
          Package          Current  Wanted  Latest  Location
          gulp-sass          2.3.2   2.3.2   3.1.0  bootstrap_4_defaulttheme

          modification example:
          "gulp-ruby-sass": "^2.3.2", change in "gulp-ruby-sass": "^2.1.1",

    3. command: **npm update** - will update module tu last version.


MAINTAINERS
-----------
  * [Jhon Obreja (jonsecret)](https://www.drupal.org/u/jonsecret)
  * [Lilian Catanoi (liliancatanoi90)](https://www.drupal.org/u/liliancatanoi90)

This project has been sponsored by:
 * [OPTASY](https://www.optasy.com) is a Canadian Drupal development & web
  development company based in Toronto. In the past we provided Drupal
  development solutions for a variety of Canadian and foregin companies with
  outstanding results.
